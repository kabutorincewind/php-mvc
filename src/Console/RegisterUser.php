<?php

namespace Console;

use Controller\AbstractController;
use Database\PDOAdapterProvider;
use File\Uploader;
use Model\Model;
use Model\User;
use PDO;

error_reporting(E_ALL);
session_start();

//autoloader
$autoload = function($class){
    $matches = array();
    preg_match('/(?P<namespace>.+\\\)?(?P<class>[^\\\]+$)/', $class, $matches);
    $filepath = __DIR__.'/../'.str_replace('\\', '/', $matches['namespace']).$matches['class'].'.php';
    if(file_exists($filepath)){
        include $filepath;
        return $class;
    }
    return false;
};
spl_autoload_register($autoload);

$config = require __DIR__.'/../Config/config.php';

//database
$dbConfig = $config['db'];
$dsn = sprintf('mysql:host=%s;dbname=%s;',$dbConfig['host'],$dbConfig['schema']);
$user = $dbConfig['username'];
$pass = $dbConfig['password'];

//DI part
$dbAdapter = new PDO($dsn,$user,$pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
$fileUploader = new Uploader(['jpg','jpeg','png','gif'], 2097152, ['image/png','image/jpeg','image/gif']);
$fileUploader->savePath = __DIR__.'/../../web/upload/';
AbstractController::setFileUploader($fileUploader);
PDOAdapterProvider::setAdapter($dbAdapter);
Model::$dbAdapter = PDOAdapterProvider::getAdapter();

$user = User::createFromArray(['name'=>'admin','email'=>'ad@ad.ad','password'=>'123','avatar' => NULL]);