<?php

namespace Controller;

use Database\PDOAdapterProvider;
use File\Uploader;

abstract class AbstractController {

    protected $currentLocale;
    protected $i18n;

    protected static $fileUploader;

    public function __construct() {
        $this->i18n = require __DIR__.'/../I18N/i18n.php';
        $this->currentLocale = (array_key_exists('l', $_SESSION) && $_SESSION['l'] != '') ? $_SESSION['l'] : 'en';
    }

    /**
     * @return Uploader
     */
    public static function getFileUploader()
    {
        return static::$fileUploader;
    }

    /**
     * @param Uploader $fileUploader
     */
    public static function setFileUploader($fileUploader)
    {
        static::$fileUploader = $fileUploader;
    }

    public function render($template, $params = null){
        $templateDir = strtolower( preg_filter('~Controller\\\~','',get_called_class()) );
        $templatePath = '../src/view/'.$templateDir.'/'.$template.'.phtml';

        $i18n = $this->i18n;
        $currentLocale = $this->currentLocale;
        $l = function ($string) use ($i18n, $currentLocale){
            if($currentLocale !== 'en' && array_key_exists($string, $i18n)){
                print $i18n[$string][$currentLocale];
            }else{
                print $string;
            }
        };

        if($params && is_array($params)){
            extract($params);
        }
        ob_start();
        include $templatePath;
        $content = ob_get_clean();
        $layoutPath = '../src/view/layout/layout.phtml';
        ob_start();
        include $layoutPath;
        $content = ob_get_clean();
        return $content;
    }
}

?>
