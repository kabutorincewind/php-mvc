<?php

namespace Model;

use ErrorException;

class Feedback extends Model
{
    public $id;
    public $name;
    public $email;
    public $message;
    public $image;
    public $approved;

    public $date;

    public function __construct($id, $name, $email, $message, $image, $approved = false)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->image = $image;
        $this->approved  = $approved;
    }

    public function save()
    {
        if($this->id){
            $statement = static::$dbAdapter->prepare(
                'UPDATE Feedback
                    SET name = :name, email = :email, message = :message, image = :image, approved = :approved
                WHERE id = :id;'
            );
            $parameters = [
                ':name' => $this->name,
                ':email' => $this->email,
                ':message' => $this->message,
                ':image' => $this->image,
                ':approved' => (int) $this->approved,
                ':id' => $this->id
            ];
            $result = $statement->execute($parameters);
        }else{
            $statement = static::$dbAdapter->prepare(
                'INSERT INTO Feedback (name, email, message, image, approved)
                VALUES (:name, :email, :message, :image, :approved);'
            );
            $parameters = [
                ':name' => $this->name,
                ':email' => $this->email,
                ':message' => $this->message,
                ':image' => $this->image,
                ':approved' => (int) $this->approved
            ];
            $execResult = $statement->execute($parameters);
            if($execResult){
                $this->id = static::$dbAdapter->lastInsertId();
                $statement = static::$dbAdapter->prepare(
                    'SELECT date FROM Feedback WHERE id = :id;'
                );
                $statement->execute([':id' => $this->id]);
                $this->date = $statement->fetch(\PDO::FETCH_ASSOC)['created'];
            }else{
                throw new ErrorException(
                    static::$dbAdapter->errorInfo()[2],
                    static::$dbAdapter->errorCode()
                );
            }
        }
        return $this;
    }
    public function delete()
    {
        static::$dbAdapter->prepare('DELETE FROM Feedback WHERE id = :id;')
            ->execute([':id' => $this->id]);
    }
    public static function getById($id)
    {
        $id = (int) $id;
        $statement = static::$dbAdapter->prepare('SELECT * FROM Feedback WHERE id = :id LIMIT 1;');
        $statement->execute([':id' => $id]);
        $userData = $statement->fetch(\PDO::FETCH_ASSOC);
        if($userData){
            $feedback = new static(
                $userData['id'], $userData['name'], $userData['email'],
                $userData['message'], $userData['image'], $userData['approved']
            );
            $feedback->date = $userData['date'];
            return $feedback;
        }
        return false;
    }

    public static function createFromArray(array $data)
    {
        $feedback = new static(
            null, $data['name'], $data['email'],
            $data['message'], $data['image'], false
        );
        $feedback->save();
        return $feedback;
    }

    public static function getAll($approved = false)
    {
        $query = 'SELECT * FROM Feedback';
        if($approved) $query.=' WHERE approved = 1';
        $query.=';';
        $statement = static::$dbAdapter->prepare($query);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
        if($data){
            foreach($data as $record){
                $result[] = new static(
                    $record['id'], $record['name'], $record['email'],
                    $record['message'], $record['image'], $record['approved']
                );
                $result[count($result)-1]->date = $record['date'];
            }
            return $result;
        }
        return false;
    }
}