<?php
namespace Model;

use ErrorException;

class User extends Model{

    public $id;
    public $name;
    public $email;
    public $passHash;
    public $avatar;
    public $token;

    public $date;

    public function __construct($id, $name, $email, $passHash, $avatar, $token)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->passHash = $passHash;
        $this->avatar = $avatar;
        $this->token = $token;
    }
    public function login($rememberMe = false)
    {
        $this->token = md5(uniqid().$this->passHash);
        $_SESSION['token'] = $this->token;
        if ($rememberMe) setcookie('token', $this->token, time()+(60*60*24*7));
        $this->save();
    }
    public function logout()
    {
        $this->token = null;
        setcookie ('token', "", time() - 3600);
        if (array_key_exists('token', $_SESSION)) unset($_SESSION['token']);
        $this->save();
    }
    public function save()
    {
        if($this->id){
            $statement = static::$dbAdapter->prepare(
                'UPDATE User
                    SET name = :name, email = :email, passHash = :passHash, avatar = :avatar, token = :token
                WHERE id = :id;'
            );
            $statement->execute([
                ':name' => $this->name,
                ':email' => $this->email,
                ':passHash' => $this->passHash,
                ':avatar' => $this->avatar,
                ':token' => $this->token,
                ':id' => $this->id
            ]);
        }else{
            $statement = static::$dbAdapter->prepare(
                'INSERT INTO User (name, email, passHash, avatar, token)
                VALUES (:name, :email, :passHash, :avatar, :token);'
            );
            $execResult = $statement->execute([
                ':name' => $this->name,
                ':email' => $this->email,
                ':passHash' => $this->passHash,
                ':avatar' => $this->avatar,
                ':token' => $this->token
            ]);
            if($execResult){
                $this->id = static::$dbAdapter->lastInsertId();
                $statement = static::$dbAdapter->prepare(
                    'SELECT created FROM User WHERE id = :id;'
                );
                $statement->execute([':id' => $this->id]);
                $this->date = $statement->fetch(\PDO::FETCH_ASSOC)['date'];
            }else{
                throw new ErrorException(
                    static::$dbAdapter->errorInfo()[2],
                    static::$dbAdapter->errorCode()
                );
            }
        }
        return $this;
    }
    public function delete()
    {
        static::$dbAdapter->prepare('DELETE FROM User WHERE id = :id;')
            ->execute([':id' => $this->id]);
    }
    public static function getById($id)
    {
        $id = (int) $id;
        $statement = static::$dbAdapter->prepare('SELECT * FROM User WHERE id = :id LIMIT 1;');
        $statement->execute([':id' => $id]);
        $userData = $statement->fetch(\PDO::FETCH_ASSOC);
        if($userData){
            return new static(
                $userData['id'], $userData['name'], $userData['email'],
                $userData['passHash'], $userData['avatar'], $userData['token']
            );
        }
        return false;
    }
    public static function getByToken($token)
    {
        $token = (string) $token;
        $statement = static::$dbAdapter->prepare('SELECT * FROM User WHERE token = :token LIMIT 1;');
        $statement->execute([':token' => $token]);
        $userData = $statement->fetch(\PDO::FETCH_ASSOC);
        if($userData){
            return new static(
                $userData['id'], $userData['name'], $userData['email'],
                $userData['passHash'], $userData['avatar'], $userData['token']
            );
        }
    }
    public static function getByCredentials($email, $password)
    {
        $email = (string) $email;
        $password = (string) $password;
        $statement = static::$dbAdapter->prepare('SELECT * FROM User WHERE email = :email LIMIT 1;');
        $statement->execute([':email' => $email]);
        $userData = $statement->fetch(\PDO::FETCH_ASSOC);
        if(password_verify($password, $userData['passHash'])){
            return new static(
                $userData['id'], $userData['name'], $userData['email'],
                $userData['passHash'], $userData['avatar'], $userData['token']
            );
        }
        return false;
    }
    public static function createFromArray(array $userData)
    {
        $userData['passHash'] = password_hash($userData['password'], PASSWORD_DEFAULT);
        $user = new static(
            null, $userData['name'], $userData['email'],
            $userData['passHash'], $userData['avatar'], null
        );
        $user->save();
        return $user;
    }

    public function hydrate($userData)
    {
        $this->id = $userData['id'];
        $this->name = $userData['name'];
        $this->email = $userData['email'];
        $this->passHash = $userData['passHash'];
        $this->avatar = $userData['avatar'];
        $this->token = $userData['token'];
        return $this;
    }
    public function extract()
    {
        $userData = [];
        $userData['id'] = $this->id;
        $userData['name'] = $this->name;
        $userData['email'] = $this->email;
        $userData['passHash'] = $this->passHash;
        $userData['avatar'] = $this->avatar;
        $userData['token'] = $this->token;
        return $userData;
    }
}

?>
